"""
Unit testing of the automatic batch processing application
"""
import unittest
from src.app import Matortue


class AppTests(unittest.TestCase):
    def test_app(self):
        """Simple Tests"""
        matortue=Matortue()
        matortue.move(10)
        self.assertEqual(matortue.getposx(), 10)
        self.assertNotEqual(matortue.getposy(), 10)



def suite():
    _suite = unittest.TestSuite()
    _suite.addTest(AppTests('test_app'))
    
    return _suite


if __name__ == "__main__":
    runner = unittest.TextTestRunner()
    runner.run(suite())
